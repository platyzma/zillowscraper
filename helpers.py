import datetime
from constants import *
from freeproxy import Proxy

def change_proxy(old_proxy=None):
    now = datetime.datetime.now()
    if old_proxy:
        old_proxy.cooldown_time = now
        old_proxy.save()

    ten_min_ago = now - datetime.timedelta(seconds=COOLDOWN_TIME)
    query = Proxy.select().where((Proxy.cooldown_time > ten_min_ago) | (Proxy.cooldown_time.is_null(True)))
    proxy = random.choice(query)
    return proxy