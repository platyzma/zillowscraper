import logging
import os
import random
import signal
import sys
import time

from PySide2.QtCore import QSettings
from fake_useragent import UserAgent
from urllib.parse import (
        urlencode, unquote, urlparse, parse_qsl, ParseResult
    )
from json import dumps

from peewee import SqliteDatabase


DEBUG_BUILD = True
LOG_FILENAME = 'logfile'
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
DEFAULT_DIR_KEY = 'default_dir'
GLOBAL_TIMEOUT = 6
DB_NAME = 'data/zillow.db'
STATES = [('OH', 'Ohio')]
COOLDOWN_TIME = 10 * 60

settings = QSettings()
logger = logging.getLogger(__name__)
db = SqliteDatabase(DB_NAME)
ua = UserAgent()



class GracefulKiller:
    kill_now = False
    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)
    #signal.signal(signal.SIGKILL, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        self.kill_now = True


killer = GracefulKiller()

class LoggerWriter(object):
    def __init__(self, logger, log_level=logging.INFO):
        self.logger = logger
        self.log_level = log_level
        self.linebuf = ''

    def write(self, buf):
        for line in buf.rstrip().splitlines():
            self.logger.log(self.log_level, line.rstrip())

    def flush(self):
        pass


def handle_exception(exc_type, exc_value, exc_traceback):
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logger.critical("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))


def init_logging():

    sys.excepthook = handle_exception

    root = logging.getLogger()
    #root.setLevel(logging.DEBUG)

    #logging.basicConfig(stream=sys.stdout)

    formatter = logging.Formatter('%(asctime)s  %(levelname)-8s [%(funcName)s:%(lineno)d]  %(message)s')
    logger.setLevel(logging.DEBUG)
    #logger.propagate = 0

    if DEBUG_BUILD:
        file_handler = logging.FileHandler(LOG_FILENAME, encoding='utf-8')
        file_handler.setLevel(logging.INFO)
        file_handler.setFormatter(formatter)
        logger.addHandler(file_handler)
        logger.info('\n---------\nLog started on %s.\n---------\n' % time.asctime())

    '''console_handler = logging.StreamHandler()
    #console_handler = ColorHandler()
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)'''

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    root.addHandler(ch)

    #sys.stdout = LoggerWriter(logger, logging.INFO)
    #sys.stderr = LoggerWriter(logger, logging.ERROR)
