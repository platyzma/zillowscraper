from peewee import *
from constants import *


def init_main_db():
    logger.info("Init Main DB")
    db.create_tables([State, County, Zipcode, Proxy, Property], safe=True)


class BaseModel(Model):
    class Meta:
        database = db


class State(BaseModel):
    shortcode = CharField(unique=True)
    name = CharField()
    rid = IntegerField(null=True)
    rtype = IntegerField(null=True)
    rect = CharField(null=True)


class County(BaseModel):
    name = CharField()
    rid = IntegerField(null=True)
    rtype = IntegerField(null=True)
    rect = CharField(null=True)
    state = ForeignKeyField(State, backref='counties')


class Zipcode(BaseModel):
    zip = CharField(unique=True)
    rid = IntegerField(null=True)
    rtype = IntegerField(null=True)
    rect = CharField(null=True)
    county = ForeignKeyField(County, backref='zipcodes')


class Proxy(BaseModel):
    proxy = CharField(primary_key=True)  # "ip:port"
    check_time = DateTimeField(null=True)  # time of testing
    response_time = FloatField(null=True)  # response time
    status_code = IntegerField(null=True)  # status code
    cooldown_time = DateTimeField(null=True)
    anonymous = BooleanField(null=True)


class Property(BaseModel):
    zpid = IntegerField(primary_key=True)
    streed_adress = CharField(null=True)
    zipcode_adress = CharField(null=True)
    city = CharField(null=True)
    price = CharField(null=True)
    bathrooms = FloatField(null=True)
    bedrooms = FloatField(null=True)
    lotsize = FloatField(null=True)
    image_link = CharField(null=True)
    zipcode = ForeignKeyField(Zipcode, backref='próperties', null=True)
    phone = CharField(null=True)
