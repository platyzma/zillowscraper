import datetime
from json import JSONDecodeError
from requests.exceptions import ProxyError, ConnectTimeout, ReadTimeout, SSLError
from uszipcode import SearchEngine
from constants import *
from db import Zipcode, County, State
import requests as r

from helpers import change_proxy

url_getZIPId = 'https://mortgageapi.zillow.com/getZIPCodeLocation?partnerId=RD-CZMBMCZ&locationRef.zipCode='
url_getRegion = 'https://www.zillow.com/search/GetRegionSelection.htm?regionId='


def build_states_db():
    """
    Add all States, Counties and Zipcodes to the Database (based on uszipcode library)
    :return:
    """
    search = SearchEngine(simple_zipcode=True)
    for shortcode, state_name in STATES:

        state, created = State.get_or_create(name=state_name, shortcode=shortcode)
        for zipcode_dict in search.by_state(state=shortcode, returns=0):
            zipcode_dict = zipcode_dict.to_dict()
            #logger.info(zipcode_dict.get('county'))
            county_name = zipcode_dict.get('county')
            zip = zipcode_dict.get('zipcode')
            county, created = County.get_or_create(name=county_name, state=state)
            Zipcode.get_or_create(zip=zip, county=county)

    logger.debug("Finished building States DB.")


def update_states_config(force_update=False):
    """
    Get id, rect & type for every zipcode/county/state via connecting to Zillow.com
    :param force_update: Don't check for null values - update it anyways.
    :return:
    """
    s = r.Session()
    user_agent = ua.random
    s.headers.update({'User-Agent': user_agent})
    # s.get('https://www.zillow.com/homes/')
    p = change_proxy()
    s.proxies.update({'http': 'http://' + p.proxy, 'https': 'https://' + p.proxy})

    for state in State.select():

        for county in state.counties:

            for zipcode in county.zipcodes:

                if zipcode.rid is None or state.rid is None or county.rid is None or force_update:
                    logger.debug("Getting Connect Info ({} - {})".format(zipcode.zip, county.name))
                    while True:
                        try:
                            response = s.get(url_getZIPId + zipcode.zip, timeout=GLOBAL_TIMEOUT).json()

                            if zipcode.rid is None or force_update:
                                zipcode.rid = int(response['location'].get('zipCodeRegionId'))
                                zipcode.save()
                            if state.rid is None or force_update:
                                state.rid = int(response['location'].get('stateRegionId'))
                                state.save()
                            if county.rid is None or force_update:
                                county.rid = int(response['location'].get('countyRegionId'))
                                county.save()
                            break
                        except (ProxyError, ConnectTimeout, ReadTimeout, SSLError):
                            logger.info("Proxy Error. Try new IP")
                            p = change_proxy(p)
                            s.proxies.update({'http': 'http://' + p.proxy, 'https': 'https://' + p.proxy})
                            continue
                        except KeyError:
                            if response.get('error') == 'InvalidLocation':
                                logger.warning("Invalid Location: " + url_getZIPId + zipcode.zip)
                                zipcode.delete_instance()
                                next_one = True
                                break
                            else:
                                logger.error("Key ERROR?!?!?!")
                                logger.exception(response)
                            p = change_proxy(p)
                            s.proxies.update({'http': 'http://' + p.proxy, 'https': 'https://' + p.proxy})
                            continue

                    if next_one:
                        continue

                if zipcode.rtype is None or zipcode.rect is None or force_update:
                    while True:
                        try:
                            region_selection = s.get(url_getRegion + str(zipcode.rid), timeout=GLOBAL_TIMEOUT)
                            region_selection = region_selection.json()
                            break
                        except (ProxyError, ConnectTimeout, ReadTimeout, SSLError):
                            p = change_proxy(p)
                            logger.info("Proxy Error. Try new IP ({})".format(p.proxy))
                            s.proxies.update({'http': 'http://' + p.proxy, 'https': 'https://' + p.proxy})
                            continue
                        except JSONDecodeError:
                            p = change_proxy(p)
                            logger.info("Captcha Error. Try new IP ({})".format(p.proxy))
                            s.proxies.update({'http': 'http://' + p.proxy, 'https': 'https://' + p.proxy})
                            continue

                    if zipcode.rect is None or force_update:
                        rect = region_selection.get('boundingRect')
                        rect_string = '{},{},{},{}'.format(rect['sw'].get('lon'), rect['sw'].get('lat'),
                                                           rect['ne'].get('lon'), rect['ne'].get('lat'))
                        zipcode.rect = rect_string
                        zipcode.save()

                    if zipcode.rtype is None or force_update:
                        zipcode.rtype = int(region_selection.get('regionType'))
                        zipcode.save()

            if county.rect is None or county.rtype is None or force_update:

                while True:
                    try:
                        region_selection = s.get(url_getRegion + str(county.rid), timeout=GLOBAL_TIMEOUT).json()
                        break
                    except (ProxyError, ConnectTimeout, ReadTimeout, SSLError):
                        p = change_proxy(p)
                        logger.info("Proxy Error. Try new IP ({})".format(p.proxy))
                        s.proxies.update({'http': 'http://' + p.proxy, 'https': 'https://' + p.proxy})
                        continue
                    except JSONDecodeError:
                        p = change_proxy(p)
                        logger.info("Captcha Error. Try new IP ({})".format(p.proxy))
                        s.proxies.update({'http': 'http://' + p.proxy, 'https': 'https://' + p.proxy})
                        continue

                if county.rect is None or force_update:
                    rect = region_selection.get('boundingRect')
                    rect_string = '{},{},{},{}'.format(rect['sw'].get('lon'), rect['sw'].get('lat'),
                                                       rect['ne'].get('lon'), rect['ne'].get('lat'))
                    county.rect = rect_string
                    county.save()

                if county.rtype is None or force_update:
                    county.rtype = int(region_selection.get('regionType'))
                    county.save()

        if state.rect is None or state.rtype is None or force_update:

            logger.debug("Updating rect/rtype of State {} {}".format(state.name, str(state.rid)))

            while True:
                try:
                    region_selection = s.get(url_getRegion + str(state.rid), timeout=GLOBAL_TIMEOUT).json()
                    break
                except (ProxyError, ConnectTimeout, ReadTimeout, SSLError):
                    p = change_proxy(p)
                    logger.info("Proxy Error. Try new IP ({})".format(p.proxy))
                    s.proxies.update({'http': 'http://' + p.proxy, 'https': 'https://' + p.proxy})
                    continue
                except JSONDecodeError:
                    p = change_proxy(p)
                    logger.info("Captcha Error. Try new IP ({})".format(p.proxy))
                    s.proxies.update({'http': 'http://' + p.proxy, 'https': 'https://' + p.proxy})
                    continue

            if state.rect is None or force_update:
                rect = region_selection.get('boundingRect')
                rect_string = '{},{},{},{}'.format(rect['sw'].get('lon'), rect['sw'].get('lat'),
                                                   rect['ne'].get('lon'), rect['ne'].get('lat'))
                state.rect = rect_string
                state.save()

            if state.rtype is None or force_update:
                state.rtype = int(region_selection.get('regionType'))
                state.save()